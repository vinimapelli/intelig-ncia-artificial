import copy
def testeObjetivo(no):
    if no['estado'] == obj:
        return True
    else:
        return False

def buscaCustoUniforme(raiz):
    print("Custo Uniforme")
    lista=[]
    lista.append(raiz)
    while len(lista) > 0:
        lista = sorted(lista, key = lambda lista: lista['custo'],reverse=True)
        print("Lista", lista)
        no=lista.pop()
        print("Elemento da Lista",no)
        if(testeObjetivo(no)):
            return no
        else:
            print("No indo gerar Sucessor",no)
            sucessor(copy.deepcopy(no), lista)

def buscaEmProfundidadeLimitada(raiz,limite):
    print("Profundidade Limitada")
    cont=-1
    lista=[]
    lista.append(raiz)
    print("Lista",lista)
    while len(lista)>0:
        no = lista.pop()
        cont=cont+1
        print("ELEMENTO DA LISTA",no)
        if(testeObjetivo(no)):
            print(no)
            print(len(lista)+cont)
            return no
        else:
            if(no['profundidade'] < limite):
                print("No indo gerar sucessor:",no)
                sucessor(copy.deepcopy(no),lista)

def buscaAprofundamentoIterativo(raiz):
    li = 0
    print("Aprofundamento Iterativo")
    retorno = buscaEmProfundidadeLimitada(raiz,li)
    while retorno == None:
        li+=1
        retorno = buscaEmProfundidadeLimitada(raiz, li)
    return retorno

def buscaEmProfundidade(raiz):
    print("Profundidade")
    lista = []
    lista.append(raiz)
    print("Lista", lista)
    while len(lista) > 0:
        no = lista.pop()
        print("ELEMENTO DA LISTA", no)
        if (testeObjetivo(no)):
            print(no)
            return no
        else:
            print("No indo gerar sucessor:", no)
            sucessor(copy.deepcopy(no), lista)


def buscaEmLargura(raiz):
    lista=[]
    lista.append(raiz)
    for element in lista:
        print("Lista",lista)
        no = element
        print("ELEMENTO DA LISTA",no)
        if(testeObjetivo(no)):
            return no
        else:
            print("No indo gerar sucessor:",no)
            sucessor(copy.deepcopy(no),lista)
            #input()

def sucessor(no,lista):
    retorno = {
        'estado': '',
        'pai': '',
        'acao': '',
        'profundidade': '',
        'custo': ''
    }
    p = position(no)
    if(p < 3):
        #print("a")
        if(p == 0):
            #print("0")
            retorno['estado'] = up(p, copy.deepcopy(no['estado']))
            retorno['pai']=no
            retorno['acao'] = "up"
            retorno['profundidade'] = no['profundidade']+1
            retorno['custo'] = no['custo']+1
            lista.append(copy.deepcopy(retorno))

            retorno['estado'] = left(p, copy.deepcopy(no['estado']))
            retorno['pai'] = no
            retorno['acao'] = "left"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            lista.append(copy.deepcopy(retorno))
        elif(p == 1):
            #print("1")
            retorno['estado'] = up(p, copy.deepcopy(no['estado']))
            retorno['pai'] = no
            retorno['acao'] = "up"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            lista.append(copy.deepcopy(retorno))

            retorno['estado'] = left(p, copy.deepcopy(no['estado']))
            retorno['pai'] = no
            retorno['acao'] = "left"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            lista.append(copy.deepcopy(retorno))

            retorno['estado'] = right(p, copy.deepcopy(no['estado']))
            retorno['pai'] = no
            retorno['acao'] = "right"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            lista.append(copy.deepcopy(retorno))

        elif(p == 2):
            #print("2")
            retorno['estado'] = up(p, copy.deepcopy(no['estado']))
            retorno['pai'] = no
            retorno['acao'] = "up"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            lista.append(copy.deepcopy(retorno))

            retorno['estado'] = right(p, copy.deepcopy(no['estado']))
            retorno['pai'] = no
            retorno['acao'] = "right"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            lista.append(copy.deepcopy(retorno))
    elif (p < 6):
        #print("b")
        if(p == 3):
            #print("0")
            retorno['estado'] = up(p,copy.deepcopy(no['estado']))
            retorno['pai'] = no
            retorno['acao'] = "up"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            lista.append(copy.deepcopy(retorno))

            retorno['estado'] = down(p,copy.deepcopy(no['estado']))
            retorno['pai'] = no
            retorno['acao'] = "down"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            lista.append(copy.deepcopy(retorno))

            retorno['estado'] = left(p,copy.deepcopy(no['estado']))
            retorno['pai'] = no
            retorno['acao'] = "left"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            lista.append(copy.deepcopy(retorno))

        elif(p == 4):
            #print("1")
            retorno['estado'] = up(p, copy.deepcopy(no['estado']))
            retorno['pai']=no
            retorno['acao'] = "up"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            lista.append(copy.deepcopy(retorno))

            retorno['estado'] = down(p, copy.deepcopy(no['estado']))
            retorno['pai']=no
            retorno['acao'] = "down"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            lista.append(copy.deepcopy(retorno))

            retorno['estado'] = left(p, copy.deepcopy(no['estado']))
            retorno['pai']=no
            retorno['acao'] = "left"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            lista.append(copy.deepcopy(retorno))

            retorno['estado'] = right(p, copy.deepcopy(no['estado']))
            retorno['pai']=no
            retorno['acao'] = "right"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            lista.append(copy.deepcopy(retorno))

        elif(p == 5):
            #print("2")
            retorno['estado'] = up(p, copy.deepcopy(no['estado']))
            retorno['pai']=no
            retorno['acao'] = "up"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            lista.append(copy.deepcopy(retorno))

            retorno['estado'] = down(p, copy.deepcopy(no['estado']))
            retorno['pai']=no
            retorno['acao'] = "down"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            lista.append(copy.deepcopy(retorno))

            retorno['estado'] = right(p, copy.deepcopy(no['estado']))
            retorno['pai']=no
            retorno['acao'] = "right"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            lista.append(copy.deepcopy(retorno))
    else:
        #print("c")
        if(p == 6):
           # print("0")
            retorno['estado'] = down(p, copy.deepcopy(no['estado']))
            retorno['pai']=no
            retorno['acao'] = "down"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            lista.append(copy.deepcopy(retorno))

            retorno['estado'] = left(p, copy.deepcopy(no['estado']))
            retorno['pai']=no
            retorno['acao'] = "left"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            lista.append(copy.deepcopy(retorno))

        elif(p == 7):
            #print("1")
            retorno['estado'] = down(p, copy.deepcopy(no['estado']))
            retorno['pai']=no
            retorno['acao'] = "down"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            lista.append(copy.deepcopy(retorno))

            retorno['estado'] = left(p, copy.deepcopy(no['estado']))
            retorno['pai']=no
            retorno['acao'] = "left"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            lista.append(copy.deepcopy(retorno))

            retorno['estado'] = right(p, copy.deepcopy(no['estado']))
            retorno['pai']=no
            retorno['acao'] = "right"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            lista.append(copy.deepcopy(retorno))

        elif(p == 8):
            #print("2")
            retorno['estado'] = down(p, copy.deepcopy(no['estado']))
            retorno['pai']=no
            retorno['acao'] = "down"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            lista.append(copy.deepcopy(retorno))

            retorno['estado'] = right(p, copy.deepcopy(no['estado']))
            retorno['pai']=no
            retorno['acao'] = "right"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            lista.append(copy.deepcopy(retorno))

def position(no):
    i = 0
    for l in no['estado']:
        if l != 9:
            i += 1
        else:
            break
    return i

def up(posicao,q):
    q[posicao]=q[posicao+3]
    q[posicao + 3] = 9
    return q

def down(posi,qu):
    qu[posi] = qu[posi-3]
    qu[posi - 3] = 9
    return qu

def right(posi,q):
    q[posi]=q[posi-1]
    q[posi-1]=+9
    return q

def left(posi,q):
    q[posi] = q[posi + 1]
    q[posi + 1] = 9
    return q

       #0 1 2

a = {
    'estado':[1,2,9,4,5,3,7,8,6],
    'acao':'',
    'pai':{},
    'profundidade':0,
    'custo':0,
    'heuristica1':0,
    'heuristica2':0
}

c = {
    'estado':[1,2,3,4,9,6,7,5,8],
    'acao':'',
    'pai':{},
    'profundidade':0,
    'custo':0,
    'heuristica1':0,
    'heuristica2':0
}

d = {
    'estado':[1,2,3,4,5,6,9,7,8],
    'acao':'',
    'pai':{},
    'profundidade':0,
    'custo':0,
    'heuristica1':0,
    'heuristica2':0
}

e = {
    'estado':[1,2,3,4,9,6,7,5,8],
    'acao':'',
    'pai':{},
    'profundidade':0,
    'custo':0,
    'heuristica1':0,
    'heuristica2':0
}

b={
    'estado':[1,2,3,4,5,6,7,8,9],
    'acao':'',
    'pai':{},
    'profundidade':0,
    'custo':0
}
obj = [1,2,3,4,5,6,7,8,9]

print('Informe qual algoritmo deseja utilizar: ')
print('1: Busca em largura: ')
print('2: Busca em profundidade: ')
print('3: Busca em profundidade Limitado: ')
print('4: Busca em Aprofundamento interativo: ')
print('5: Custo Uniforme: ')
op = int(input('Informe uma opação: '))
if(op == 1):
    print("Solução:",buscaEmLargura(a))
elif(op == 2):
    print("Solução:",buscaEmProfundidade(a))
elif(op == 3):
    print("Limite:")
    pr = int(input())
    print("Solução:",buscaEmProfundidadeLimitada(a,pr))
elif(op == 4):
    print("Solução:",buscaAprofundamentoIterativo(a))
    input()
    print("Solução:", buscaAprofundamentoIterativo(c))
    input()
    print("Solução:", buscaAprofundamentoIterativo(d))
    input()
    print("Solução:", buscaAprofundamentoIterativo(e))
elif(op == 5):
    print("Solução:",buscaCustoUniforme(a))