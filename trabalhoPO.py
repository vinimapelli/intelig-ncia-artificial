import copy

def testeObjetivo(no):
    if no['estado'] == obj:
        return True
    else:
        return False


def buscaGMEH1(raiz):
    print("GME COM H1")
    cont=1
    gerado = -1
    lista = []
    lista.append(raiz)
    while len(lista) > 0:
        print("\nInteracao",cont)
        print(lista)
        lista = sorted(lista, key=lambda lista: lista['heuristica1'], reverse=True)
        print("Lista", lista)
        no = lista.pop()
        gerado=gerado+1
        print("ELEMENTO DA LISTA", no)
        if (testeObjetivo(no)):
            print(len(lista)+gerado)
            return no
        elif no['profundidade'] == 20:
            print(len(lista) + gerado)
            return no
        else:
            print("No indo gerar sucessor:", no)
            sucessor(copy.deepcopy(no), lista)
            # input()
        cont = cont+1

def buscaGMEH2(raiz):
    print("GME com H2")
    cont = 1
    gerado = -1
    lista = []
    lista.append(raiz)
    while len(lista) > 0:
        print("\nInteracao", cont)
        print(lista)
        lista = sorted(lista, key=lambda lista: lista['heuristica2'], reverse=True)
        print("Lista", lista)
        no = lista.pop()
        gerado = gerado + 1
        print("ELEMENTO DA LISTA", no)
        if (testeObjetivo(no)):
            print(len(lista)+gerado)
            return no
        elif no['profundidade'] == 20:
            print(len(lista) + gerado)
            return no
        else:
            print("No indo gerar sucessor:", no)
            sucessor(copy.deepcopy(no), lista)
            # input()
        cont = cont+1

def buscaAH1(raiz):
    print("A* com H1")
    cont = 1
    gerado = -1
    lista = []
    lista.append(raiz)
    while len(lista) > 0:
        print("\nInteracao", cont)
        print(lista)
        lista = sorted(lista, key=lambda lista: (lista['heuristica1'] + lista['custo']), reverse=True)
        print("Lista", lista)
        no = lista.pop()
        gerado = gerado +1
        print("ELEMENTO DA LISTA", no)
        if (testeObjetivo(no)):
            print(len(lista)+gerado)
            return no
        else:
            print("No indo gerar sucessor:", no)
            sucessor(copy.deepcopy(no), lista)
            # input()
        cont = cont + 1

def buscaAH2(raiz):
    print("A* com H2")
    cont = 1
    gerado = -1
    lista = []
    lista.append(raiz)
    while len(lista) > 0:
        print("\nInteracao", cont)
        print(lista)
        lista = sorted(lista, key=lambda lista: (lista['heuristica2'] + lista['custo']), reverse=True)
        print("Lista", lista)
        no = lista.pop()
        gerado = gerado +1
        print("ELEMENTO DA LISTA", no)
        if (testeObjetivo(no)):
            print(len(lista)+gerado)
            return no
        else:
            print("No indo gerar sucessor:", no)
            sucessor(copy.deepcopy(no), lista)
            # input()
        cont = cont + 1


def sucessor(no,lista):
    retorno = {
        'estado': '',
        'pai': '',
        'acao': '',
        'profundidade': '',
        'custo': '',
        'heuristica1':'',
        'heuristica2':''
    }
    p = position(no)
    if(p < 3):
        #print("a")
        if(p == 0):
            #print("0")
            retorno['estado'] = up(p, copy.deepcopy(no['estado']))
            retorno['pai']=no
            retorno['acao'] = "up"
            retorno['profundidade'] = no['profundidade']+1
            retorno['custo'] = no['custo']+1
            retorno['heuristica1'] = h1(copy.deepcopy(retorno['estado']))
            retorno['heuristica2'] = h2(copy.deepcopy(retorno['estado']))
            lista.append(copy.deepcopy(retorno))

            retorno['estado'] = left(p, copy.deepcopy(no['estado']))
            retorno['pai'] = no
            retorno['acao'] = "left"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            retorno['heuristica1'] = h1(copy.deepcopy(retorno['estado']))
            retorno['heuristica2'] = h2(copy.deepcopy(retorno['estado']))
            lista.append(copy.deepcopy(retorno))
        elif(p == 1):
            #print("1")
            retorno['estado'] = up(p, copy.deepcopy(no['estado']))
            retorno['pai'] = no
            retorno['acao'] = "up"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            retorno['heuristica1'] = h1(copy.deepcopy(retorno['estado']))
            retorno['heuristica2'] = h2(copy.deepcopy(retorno['estado']))
            lista.append(copy.deepcopy(retorno))

            retorno['estado'] = left(p, copy.deepcopy(no['estado']))
            retorno['pai'] = no
            retorno['acao'] = "left"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            retorno['heuristica1'] = h1(copy.deepcopy(retorno['estado']))
            retorno['heuristica2'] = h2(copy.deepcopy(retorno['estado']))
            lista.append(copy.deepcopy(retorno))

            retorno['estado'] = right(p, copy.deepcopy(no['estado']))
            retorno['pai'] = no
            retorno['acao'] = "right"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            retorno['heuristica1'] = h1(copy.deepcopy(retorno['estado']))
            retorno['heuristica2'] = h2(copy.deepcopy(retorno['estado']))
            lista.append(copy.deepcopy(retorno))

        elif(p == 2):
            #print("2")
            retorno['estado'] = up(p, copy.deepcopy(no['estado']))
            retorno['pai'] = no
            retorno['acao'] = "up"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            retorno['heuristica1'] = h1(copy.deepcopy(retorno['estado']))
            retorno['heuristica2'] = h2(copy.deepcopy(retorno['estado']))
            lista.append(copy.deepcopy(retorno))

            retorno['estado'] = right(p, copy.deepcopy(no['estado']))
            retorno['pai'] = no
            retorno['acao'] = "right"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            retorno['heuristica1'] = h1(copy.deepcopy(retorno['estado']))
            retorno['heuristica2'] = h2(copy.deepcopy(retorno['estado']))
            lista.append(copy.deepcopy(retorno))
    elif (p < 6):
        #print("b")
        if(p == 3):
            #print("0")
            retorno['estado'] = up(p,copy.deepcopy(no['estado']))
            retorno['pai'] = no
            retorno['acao'] = "up"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            retorno['heuristica1'] = h1(copy.deepcopy(retorno['estado']))
            retorno['heuristica2'] = h2(copy.deepcopy(retorno['estado']))
            lista.append(copy.deepcopy(retorno))

            retorno['estado'] = down(p,copy.deepcopy(no['estado']))
            retorno['pai'] = no
            retorno['acao'] = "down"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            retorno['heuristica1'] = h1(copy.deepcopy(retorno['estado']))
            retorno['heuristica2'] = h2(copy.deepcopy(retorno['estado']))
            lista.append(copy.deepcopy(retorno))

            retorno['estado'] = left(p,copy.deepcopy(no['estado']))
            retorno['pai'] = no
            retorno['acao'] = "left"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            retorno['heuristica1'] = h1(copy.deepcopy(retorno['estado']))
            retorno['heuristica2'] = h2(copy.deepcopy(retorno['estado']))
            lista.append(copy.deepcopy(retorno))

        elif(p == 4):
            #print("1")
            retorno['estado'] = up(p, copy.deepcopy(no['estado']))
            retorno['pai']=no
            retorno['acao'] = "up"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            retorno['heuristica1'] = h1(copy.deepcopy(retorno['estado']))
            retorno['heuristica2'] = h2(copy.deepcopy(retorno['estado']))
            lista.append(copy.deepcopy(retorno))

            retorno['estado'] = down(p, copy.deepcopy(no['estado']))
            retorno['pai']=no
            retorno['acao'] = "down"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            retorno['heuristica1'] = h1(copy.deepcopy(retorno['estado']))
            retorno['heuristica2'] = h2(copy.deepcopy(retorno['estado']))
            lista.append(copy.deepcopy(retorno))

            retorno['estado'] = left(p, copy.deepcopy(no['estado']))
            retorno['pai']=no
            retorno['acao'] = "left"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            retorno['heuristica1'] = h1(copy.deepcopy(retorno['estado']))
            retorno['heuristica2'] = h2(copy.deepcopy(retorno['estado']))
            lista.append(copy.deepcopy(retorno))

            retorno['estado'] = right(p, copy.deepcopy(no['estado']))
            retorno['pai']=no
            retorno['acao'] = "right"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            retorno['heuristica1'] = h1(copy.deepcopy(retorno['estado']))
            retorno['heuristica2'] = h2(copy.deepcopy(retorno['estado']))
            lista.append(copy.deepcopy(retorno))

        elif(p == 5):
            #print("2")
            retorno['estado'] = up(p, copy.deepcopy(no['estado']))
            retorno['pai']=no
            retorno['acao'] = "up"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            retorno['heuristica1'] = h1(copy.deepcopy(retorno['estado']))
            retorno['heuristica2'] = h2(copy.deepcopy(retorno['estado']))
            lista.append(copy.deepcopy(retorno))

            retorno['estado'] = down(p, copy.deepcopy(no['estado']))
            retorno['pai']=no
            retorno['acao'] = "down"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            retorno['heuristica1'] = h1(copy.deepcopy(retorno['estado']))
            retorno['heuristica2'] = h2(copy.deepcopy(retorno['estado']))
            lista.append(copy.deepcopy(retorno))

            retorno['estado'] = right(p, copy.deepcopy(no['estado']))
            retorno['pai']=no
            retorno['acao'] = "right"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            retorno['heuristica1'] = h1(copy.deepcopy(retorno['estado']))
            retorno['heuristica2'] = h2(copy.deepcopy(retorno['estado']))
            lista.append(copy.deepcopy(retorno))
    else:
        #print("c")
        if(p == 6):
           # print("0")
            retorno['estado'] = down(p, copy.deepcopy(no['estado']))
            retorno['pai']=no
            retorno['acao'] = "down"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            retorno['heuristica1'] = h1(copy.deepcopy(retorno['estado']))
            retorno['heuristica2'] = h2(copy.deepcopy(retorno['estado']))
            lista.append(copy.deepcopy(retorno))

            retorno['estado'] = left(p, copy.deepcopy(no['estado']))
            retorno['pai']=no
            retorno['acao'] = "left"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            retorno['heuristica1'] = h1(copy.deepcopy(retorno['estado']))
            retorno['heuristica2'] = h2(copy.deepcopy(retorno['estado']))
            lista.append(copy.deepcopy(retorno))

        elif(p == 7):
            #print("1")
            retorno['estado'] = down(p, copy.deepcopy(no['estado']))
            retorno['pai']=no
            retorno['acao'] = "down"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            retorno['heuristica1'] = h1(copy.deepcopy(retorno['estado']))
            retorno['heuristica2'] = h2(copy.deepcopy(retorno['estado']))
            lista.append(copy.deepcopy(retorno))

            retorno['estado'] = left(p, copy.deepcopy(no['estado']))
            retorno['pai']=no
            retorno['acao'] = "left"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            retorno['heuristica1'] = h1(copy.deepcopy(retorno['estado']))
            retorno['heuristica2'] = h2(copy.deepcopy(retorno['estado']))
            lista.append(copy.deepcopy(retorno))

            retorno['estado'] = right(p, copy.deepcopy(no['estado']))
            retorno['pai']=no
            retorno['acao'] = "right"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            retorno['heuristica1'] = h1(copy.deepcopy(retorno['estado']))
            retorno['heuristica2'] = h2(copy.deepcopy(retorno['estado']))
            lista.append(copy.deepcopy(retorno))

        elif(p == 8):
            #print("2")
            retorno['estado'] = down(p, copy.deepcopy(no['estado']))
            retorno['pai']=no
            retorno['acao'] = "down"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            retorno['heuristica1'] = h1(copy.deepcopy(retorno['estado']))
            retorno['heuristica2'] = h2(copy.deepcopy(retorno['estado']))
            lista.append(copy.deepcopy(retorno))

            retorno['estado'] = right(p, copy.deepcopy(no['estado']))
            retorno['pai']=no
            retorno['acao'] = "right"
            retorno['profundidade'] = no['profundidade'] + 1
            retorno['custo'] = no['custo'] + 1
            retorno['heuristica1'] = h1(copy.deepcopy(retorno['estado']))
            retorno['heuristica2'] = h2(copy.deepcopy(retorno['estado']))
            lista.append(copy.deepcopy(retorno))


def position(no):
    i = 0
    for l in no['estado']:
        if l != 9:
            i += 1
        else:
            break
    return i

def up(posicao,q):
    q[posicao]=q[posicao+3]
    q[posicao + 3] = 9
    return q

def down(posi,qu):
    qu[posi] = qu[posi-3]
    qu[posi - 3] = 9
    return qu

def right(posi,q):
    q[posi]=q[posi-1]
    q[posi-1]=+9
    return q

def left(posi,q):
    q[posi] = q[posi + 1]
    q[posi + 1] = 9
    return q



#############################
# HEURÍSTICAS
#############################
# Número de peças fora do lugar
#[1,2,3,4,5,6,9,7,8]
def h1(tab):
    a = 0
    if tab[0] != 1: a = a + 1
    if tab[1] != 2: a = a + 1
    if tab[2] != 3: a = a + 1
    if tab[3] != 4: a = a + 1
    if tab[4] != 5: a = a + 1
    if tab[5] != 6: a = a + 1
    if tab[6] != 7: a = a + 1
    if tab[7] != 8: a = a + 1
    if tab[8] != 9: a = a + 1
    return a


# Distancia Manhattan
#[2,3,4,
# 5,6,7
#,8,9,1]
def h2(tab):
    h = 0
    for j in range(9):
        if (tab[j] == 1):
            h = h + abs(0 - (j//3)) + abs(0 - (j%3))
        if (tab[j] == 2):
            h = h + abs(0 - (j//3)) + abs(1 - (j%3))
        if (tab[j] == 3):
            h = h + abs(0 - (j//3)) + abs(2 - (j%3))
        if (tab[j] == 4):
            h = h + abs(1 - (j//3)) + abs(0 - (j%3))
        if (tab[j] == 5):
            h = h + abs(1 - (j//3)) + abs(1 - (j%3))
        if (tab[j] == 6):
            h = h + abs(1 - (j//3)) + abs(2 - (j%3))
        if (tab[j] == 7):
            h = h + abs(2 - (j//3)) + abs(0 - (j%3))
        if (tab[j] == 8):
            h = h + abs(2 - (j//3)) + abs(1 - (j%3))
        if (tab[j] == 9):
            h = h + abs(2 - (j//3)) + abs(2 - (j%3))
    return h


a = {
    'estado':[1,2,9,4,5,3,7,8,6],
    'acao':'',
    'pai':{},
    'profundidade':0,
    'custo':0,
    'heuristica1':0,
    'heuristica2':0
}

c = {
    'estado':[1,2,3,4,9,6,7,5,8],
    'acao':'',
    'pai':{},
    'profundidade':0,
    'custo':0,
    'heuristica1':0,
    'heuristica2':0
}

d = {
    'estado':[1,2,3,4,5,6,9,7,8],
    'acao':'',
    'pai':{},
    'profundidade':0,
    'custo':0,
    'heuristica1':0,
    'heuristica2':0
}

e = {
    'estado':[1,2,3,4,9,6,7,5,8],
    'acao':'',
    'pai':{},
    'profundidade':0,
    'custo':0,
    'heuristica1':0,
    'heuristica2':0
}


obj = [1,2,3,4,5,6,7,8,9]

print('Informe qual algoritmo deseja utilizar: ')
print('1: Busca GME: ')
print('2: Busca A*: ')
op = int(input('Informe uma opação: '))
if op == 1:
    print("\nResposta GME h1:",buscaGMEH1(a))
    input()
    print("\nResposta GME h1:", buscaGMEH1(c))
    input()
    print("\nResposta GME h1:", buscaGMEH1(d))
    input()
    print("\nResposta GME h1:", buscaGMEH1(e))
    print("\n")
    input()
    print("\nResposta GME h2:", buscaGMEH2(a))
    input()
    print("\nResposta GME h2:", buscaGMEH2(c))
    input()
    print("\nResposta GME h2:", buscaGMEH2(d))
    input()
    print("\nResposta GME h2:", buscaGMEH2(e))
    print("\n")
elif op == 2:
    print("\nResposta A* h1:", buscaAH1(a))
    input()
    print("\nResposta A* h1:", buscaAH1(c))
    input()
    print("\nResposta A* h1:", buscaAH1(d))
    input()
    print("\nResposta A* h1:", buscaAH1(e))
    print("\n")
    input()
    print("\nResposta A* h2:", buscaAH2(a))
    input()
    print("\nResposta A* h2:", buscaAH2(c))
    input()
    print("\nResposta A* h2:", buscaAH2(d))
    input()
    print("\nResposta A* h2:", buscaAH2(e))
    print("\n")
